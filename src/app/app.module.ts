import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {VideoPage} from "../pages/video/video";
import {MusicPage} from "../pages/music/music";
import {NewsPage} from "../pages/news/news";
import {IntroPage} from "../pages/intro/intro";
import {LoginPage} from "../pages/login/login";
import {MusicPop} from "../pages/music/music-popOver/popover";

@NgModule({
  declarations: [
    MyApp,
    VideoPage,
    MusicPage,
    NewsPage,
    TabsPage,
    IntroPage,
    LoginPage,
    MusicPop
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    VideoPage,
    MusicPage,
    NewsPage,
    TabsPage,
    IntroPage,
    LoginPage,
    MusicPop
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
